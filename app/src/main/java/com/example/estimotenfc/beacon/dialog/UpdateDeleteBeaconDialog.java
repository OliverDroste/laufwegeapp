package com.example.estimotenfc.beacon.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.example.estimotenfc.R;

import java.util.Objects;

public class UpdateDeleteBeaconDialog extends AppCompatDialogFragment {

    private UpdateDeleteBeaconDialogListener listener;

    public UpdateDeleteBeaconDialog(String instance, String xCoordinate, String yCoordinate) {
        this.instance = instance;
        this.originalXCoordinate = xCoordinate;
        this.originalYCoordinate = yCoordinate;
    }

    private final String instance;
    private String xCoordinate;
    private String yCoordinate;
    private final String originalXCoordinate;
    private final String originalYCoordinate;
    private EditText editTextXCoordinate;
    private EditText editTextYCoordinate;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = Objects.requireNonNull(getActivity()).getLayoutInflater();
        View view = inflater.inflate(R.layout.popup_layout,null);

        builder.setView(view)
                .setTitle("Update or Delete Beacon")
                .setNegativeButton("Cancel", (dialog, which) -> {

                })
                .setPositiveButton("Update", (dialog, which) -> {
                    String xCoordinate = editTextXCoordinate.getText().toString();
                    String yCoordinate = editTextYCoordinate.getText().toString();
                    listener.updateApplyTexts(instance, xCoordinate,yCoordinate, originalXCoordinate, originalYCoordinate);
                    })
                .setNeutralButton("Delete", (dialog, which) -> listener.deleteApplyTexts(instance));

        TextView textViewInstance = view.findViewById(R.id.beaconInstancePUTv);
        textViewInstance.setText(instance);
        editTextXCoordinate = view.findViewById(R.id.xCoordinatePUEt);
        editTextYCoordinate = view.findViewById(R.id.yCoordinatePUEt);
        editTextXCoordinate.setText(originalXCoordinate);
        editTextYCoordinate.setText(originalYCoordinate);

        return builder.create();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        try {
            listener = (UpdateDeleteBeaconDialog.UpdateDeleteBeaconDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "must implement ExampleDialogListener");
        }

    }


    public interface UpdateDeleteBeaconDialogListener{

        void updateApplyTexts(String instance, String xCoordinate, String yCoordinate, String originalXCoordinate, String originalYCoordinate);
        void deleteApplyTexts(String instance);
    }

}
