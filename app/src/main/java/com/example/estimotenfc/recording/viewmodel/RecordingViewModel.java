package com.example.estimotenfc.recording.viewmodel;

import android.os.Handler;
import android.os.Looper;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.estimotenfc.beacon.data.Beacon;
import com.example.estimotenfc.recording.data.RecordedPosition;
import com.example.estimotenfc.remote.api.JsonApi;
import com.example.estimotenfc.remote.data.Session;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.time.LocalDateTime;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RecordingViewModel extends ViewModel {

    private final Retrofit retrofit;

    {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(LocalDateTime.class, new LocalDateTimeAdapter())
                .create();

        GsonConverterFactory gsonConverterFactory = GsonConverterFactory.create(gson);
        String BASEURL = "https://springbootapp-1593292906275.azurewebsites.net";
        retrofit = new Retrofit.Builder()
                    .baseUrl(BASEURL)
                    .addConverterFactory(gsonConverterFactory)
                    .build();
    }

    private final JsonApi jsonApi = retrofit.create(JsonApi.class);

    private MutableLiveData<List<Beacon>> beacons;

    public LiveData<List<Beacon>> getBeacons() {

        if(beacons==null){
            beacons = new MutableLiveData<>();
            loadBeacons();
        }
        return beacons;
    }

    private void loadBeacons() {

        Call<List<Beacon>> call = jsonApi.getBeacons();

        call.enqueue(new Callback<List<Beacon>>() {
            @Override
            public void onResponse(Call<List<Beacon>> call, Response<List<Beacon>> response) {

                if(!response.isSuccessful()) {
                    System.out.println(response.code());
                }else {

                    beacons.postValue(response.body());

                }
            }

            @Override
            public void onFailure(Call<List<Beacon>> call, Throwable t) {
                System.out.println(t.getMessage());
            }
        });
    }

    public void addSession(int userId, Session session, List<RecordedPosition> coordinatesList) {

        Call<Session> call = jsonApi.addSession(userId,session);




        call.enqueue(new Callback<Session>() {

            final Handler mainHandler = new Handler(Looper.getMainLooper());

            @Override
            public void onResponse(Call<Session> call, Response<Session> response) {

                mainHandler.post(() -> {

                    if(!response.isSuccessful()) {
                        System.out.println("AddSession HTTP Statuscode: " + response.code());
                    }else {

                        System.out.println("Session added!");
                        addCoordinates(response.body().getSessionId(),coordinatesList);

                    }
                });

            }

            @Override
            public void onFailure(Call<Session> call, Throwable t) {
                System.out.println(t.getMessage());
            }
        });
    }

    private void addCoordinates(int sessionId, List<RecordedPosition> coordinatesList) {

        Call<Void> call = jsonApi.addCoordinates(sessionId,coordinatesList);

        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {

                if(!response.isSuccessful()) {
                    System.out.println("AddCoordinates HTTP Statuscode: " + response.code());
                }else {
                    if(response.code()==200){
                        System.out.println("Coordinates added!");

                    }
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                System.out.println(t.getMessage());
            }
        });
    }




}
