package com.example.estimotenfc.recording.data;

import java.time.LocalDateTime;
import java.util.Set;

public class RecordingDataSet {

    private LocalDateTime timestamp;
    private Set<ReceivedSignal> strongestSignals;

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public Set<ReceivedSignal> getStrongestSignals() {
        return strongestSignals;
    }

    public void setStrongestSignals(Set<ReceivedSignal> strongestSignals) {
        this.strongestSignals = strongestSignals;
    }

    public RecordingDataSet(LocalDateTime timestamp, Set<ReceivedSignal> strongestSignals) {
        this.timestamp = timestamp;
        this.strongestSignals = strongestSignals;
    }

}
