package com.example.estimotenfc.recording.data;

import org.jetbrains.annotations.NotNull;

import java.time.LocalDateTime;

public class ReceivedSignal {

    private String instance;
    private LocalDateTime timestamp;
    private int rssi;
    private double distance;
    private double x;
    private double y;

    public String getInstance() {
        return instance;
    }

    public void setInstance(String instance) {
        this.instance = instance;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public int getRssi() {
        return rssi;
    }

    public void setRssi(int rssi) {
        this.rssi = rssi;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }


    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public ReceivedSignal(String instance, LocalDateTime timestamp, int rssi, double distance, double x, double y) {
        this.instance = instance;
        this.timestamp = timestamp;
        this.rssi = rssi;
        this.distance = distance;
        this.x = x;
        this.y = y;
    }


    @NotNull
    @Override
    public String toString() {
        return "ReceivedSignal{" +
                "instance='" + instance + '\'' +
                ", timestamp=" + timestamp +
                ", rssi=" + rssi +
                ", distance=" + distance +
                ", x=" + x +
                ", y=" + y +
                '}';
    }
}
