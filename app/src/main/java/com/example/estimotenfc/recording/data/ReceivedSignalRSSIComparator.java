package com.example.estimotenfc.recording.data;

import java.util.Comparator;

public class ReceivedSignalRSSIComparator implements Comparator<ReceivedSignal> {


    @Override
    public int compare(ReceivedSignal o1, ReceivedSignal o2) {

        return -Integer.compare(o1.getRssi(),o2.getRssi());

    }

    @Override
    public boolean equals(Object obj) {
        return false;
    }
}
