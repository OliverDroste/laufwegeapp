package com.example.estimotenfc.user.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.estimotenfc.R;
import com.example.estimotenfc.user.data.User;

import java.util.ArrayList;

public class UserAdapter extends ArrayAdapter<User> {

    public UserAdapter(Context context, ArrayList<User> userList){
        super(context, 0, userList);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    private View initView(int position, View convertView, ViewGroup parent) {
     if(convertView == null){
         convertView = LayoutInflater.from(getContext()).inflate(
                 R.layout.userid_spinner_row,parent,false
         );
     }
        TextView textViewUserId = convertView.findViewById(R.id.userIdSpTv);
        TextView textViewUserName = convertView.findViewById(R.id.nameSpTv);
        TextView textViewUserSurname = convertView.findViewById(R.id.surnameSpTv);

        User currentUser = getItem(position);


        if(currentUser!=null) {
            textViewUserId.setText(Integer.toString(currentUser.getUserId()));
            textViewUserName.setText(currentUser.getName());
            textViewUserSurname.setText(currentUser.getSurname());
        }
        return convertView;
    }

}
