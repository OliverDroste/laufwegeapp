package com.example.estimotenfc.recording.data;

import org.jetbrains.annotations.NotNull;

import java.time.LocalDateTime;

public class RecordedPosition {

    private LocalDateTime timestamp;
    private double xCoordinate;
    private double yCoordinate;
    private boolean isMachine;


    public RecordedPosition(LocalDateTime timestamp, double xCoordinate, double yCoordinate, boolean isMachine) {
        this.timestamp = timestamp;
        this.xCoordinate = xCoordinate;
        this.yCoordinate = yCoordinate;
        this.isMachine = isMachine;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public double getxCoordinate() {
        return xCoordinate;
    }

    public void setxCoordinate(double xCoordinate) {
        this.xCoordinate = xCoordinate;
    }

    public double getyCoordinate() {
        return yCoordinate;
    }

    public void setyCoordinate(double yCoordinate) {
        this.yCoordinate = yCoordinate;
    }

    public boolean getIsMachine() {
        return isMachine;
    }

    public void setIsMachine(boolean machine) {
        isMachine = machine;
    }

    @NotNull
    @Override
    public String toString() {
        return "RecordedPosition{" +
                "timestamp=" + timestamp +
                ", xCoordinate=" + xCoordinate +
                ", yCoordinate=" + yCoordinate +
                ", isMachine=" + isMachine +
                '}';
    }
}
