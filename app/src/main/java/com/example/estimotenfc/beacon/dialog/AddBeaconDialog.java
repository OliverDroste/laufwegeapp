package com.example.estimotenfc.beacon.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.example.estimotenfc.R;

import java.util.Objects;

public class AddBeaconDialog extends AppCompatDialogFragment {

    private AddBeaconDialogListener listener;

    public AddBeaconDialog(String instance, boolean isMachine) {
        this.instance = instance;
        this.isMachine=isMachine;
    }

    public AddBeaconDialog(String instance, String xCoordinate, String yCoordinate, boolean isMachine) {
        this.instance = instance;
        this.xCoordinate=xCoordinate;
        this.yCoordinate=yCoordinate;
        this.isMachine=isMachine;
    }

    private final String instance;
    private EditText editTextXCoordinate;
    private EditText editTextYCoordinate;
    private String xCoordinate;
    private String yCoordinate;
    private final boolean isMachine;


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = Objects.requireNonNull(getActivity()).getLayoutInflater();
        View view = inflater.inflate(R.layout.popup_layout,null);



        builder.setView(view)
                .setTitle("Add Beacon")
                .setNegativeButton("Cancel", (dialog, which) -> {

                })
                .setPositiveButton("Add", (dialog, which) -> {
                    String xCoordinate = editTextXCoordinate.getText().toString();
                    String yCoordinate = editTextYCoordinate.getText().toString();
                    listener.applyTexts(instance, xCoordinate,yCoordinate, isMachine);
                });

        TextView textViewInstance = view.findViewById(R.id.beaconInstancePUTv);
        textViewInstance.setText(instance);
        editTextXCoordinate = view.findViewById(R.id.xCoordinatePUEt);
        editTextYCoordinate = view.findViewById(R.id.yCoordinatePUEt);

        if(!(xCoordinate == null) && (!(yCoordinate == null))) {
            editTextXCoordinate.setText(xCoordinate);
            editTextYCoordinate.setText(yCoordinate);
        }

        return builder.create();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        try {
            listener = (AddBeaconDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "must implement ExampleDialogListener");
        }

    }


    public interface AddBeaconDialogListener{

        void applyTexts(String instance, String xCoordinate, String yCoordinate, boolean isMachine);

    }



}
