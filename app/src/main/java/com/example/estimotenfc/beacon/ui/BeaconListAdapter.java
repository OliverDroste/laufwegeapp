package com.example.estimotenfc.beacon.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.estimotenfc.R;
import com.example.estimotenfc.beacon.data.Beacon;

import java.util.List;
import java.util.Objects;

class BeaconListAdapter extends ArrayAdapter<Beacon> {

    private final Context context;
    private final int resource;

    BeaconListAdapter(Context context, int resource, List<Beacon> objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource =resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        //get the beacon information
        String instance = Objects.requireNonNull(getItem(position)).getBeaconId();
        double xCoordinate = Objects.requireNonNull(getItem(position)).getxCoordinate();
        double yCoordinate = Objects.requireNonNull(getItem(position)).getyCoordinate();
        boolean isMachine = Objects.requireNonNull(getItem(position)).getIsMachine();

        Beacon beacon = new Beacon(instance,xCoordinate,yCoordinate,isMachine);

        LayoutInflater inflater = LayoutInflater.from(context);
        convertView = inflater.inflate(resource,parent,false);

        TextView tvInstance = convertView.findViewById(R.id.beaconInstanceTv);
        TextView tvxCoordinate = convertView.findViewById(R.id.xCoordinateTv);
        TextView tvyCoordinate = convertView.findViewById(R.id.yCoordinateTv);

        tvInstance.setText(instance);
        tvxCoordinate.setText(Double.toString(xCoordinate));
        tvyCoordinate.setText(Double.toString(yCoordinate));

        return convertView;
    }
}
