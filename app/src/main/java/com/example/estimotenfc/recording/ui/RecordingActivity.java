package com.example.estimotenfc.recording.ui;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcManager;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.estimote.coresdk.recognition.packets.Eddystone;
import com.estimote.coresdk.recognition.utils.DeviceId;
import com.estimote.coresdk.service.BeaconManager;
import com.example.estimotenfc.R;
import com.example.estimotenfc.beacon.data.Beacon;
import com.example.estimotenfc.beacon.viewmodel.BeaconViewModel;
import com.example.estimotenfc.recording.data.ReceivedSignal;
import com.example.estimotenfc.recording.data.ReceivedSignalRSSIComparator;
import com.example.estimotenfc.recording.data.RecordedPosition;
import com.example.estimotenfc.recording.data.RecordingDataSet;
import com.example.estimotenfc.recording.dialog.SaveRecordingSessionDialog;
import com.example.estimotenfc.recording.viewmodel.RecordingViewModel;
import com.example.estimotenfc.remote.data.Session;
import com.example.estimotenfc.start.StartActivity;
import com.lemmingapex.trilateration.NonLinearLeastSquaresSolver;
import com.lemmingapex.trilateration.TrilaterationFunction;

import org.apache.commons.math3.fitting.leastsquares.LeastSquaresOptimizer;
import org.apache.commons.math3.fitting.leastsquares.LevenbergMarquardtOptimizer;
import org.jetbrains.annotations.NotNull;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class RecordingActivity extends AppCompatActivity implements SaveRecordingSessionDialog.SaveRecordingSessionDialogListener {

    private final int SCANNING_ON = 1;
    private final int SCANNING_OFF = 0;
    private final int SCANNING_PAUSED = -1;

    private NfcAdapter nfcAndroidAdapter;
    private PendingIntent nfcIntent;

    private List<RecordedPosition> positionData;

    private RecordingViewModel recordingViewModel;

    private int userId;
    private String name;
    private String surname;
    private LocalDateTime startTime;
    private LocalDateTime endTime;

    private SwipeButton swipeButton;
    private ProgressBar progressBar;
    private Button startBtn;
    private Button saveRecordingSessionBtn;
    private int btnState; //0 - no scanning, 1 - Scanning going on

    private ListView recordedPositionListView;
    private RecordedPositionListAdapter recordedPositionListAdapter;

    private Map<String, Beacon> beaconsMap;

    private BeaconManager beaconManager;

    private List<ReceivedSignal> receivedSignals;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_recording);

        swipeButton = findViewById(R.id.swipe_btn);
        recordingViewModel = new ViewModelProvider(this).get(RecordingViewModel.class);

        btnState = 0;
        saveRecordingSessionBtn = findViewById(R.id.saveRecordingSessionBtn);

        progressBar = findViewById(R.id.recordingActivityLoadingBar);
        recordedPositionListView = findViewById(R.id.recordedPositionListView);

        userId = getIntent().getIntExtra("EXTRA_USER_ID", 0);
        name = getIntent().getStringExtra("EXTRA_USER_NAME");
        surname = getIntent().getStringExtra("EXTRA_USER_SURNAME");

        beaconsMap = new HashMap<>();
        TextView transferredBeaconIdTv = findViewById(R.id.recordingActivityUserIdTv);
        transferredBeaconIdTv.setText("User ID: " + userId + " " + name + " " + surname);

        positionData = new ArrayList<>();

        BeaconViewModel beaconViewModel = new ViewModelProvider(this).get(BeaconViewModel.class);

        beaconViewModel.getBeacons().observe(this, beacons -> {

            for (Beacon beacon : beacons) {
                beaconsMap.put(beacon.getBeaconId(), beacon);
            }

        });

        startBtn = findViewById(R.id.startBtn);
        Button clearDataBtn = findViewById(R.id.clearDataBtn);

        initNfcAdapter();

        receivedSignals = new ArrayList<>();

        if (nfcAndroidAdapter == null) {
            // Device does not support NFC
            Toast.makeText(RecordingActivity.this,
                    "Device does not support NFC!",
                    Toast.LENGTH_LONG).show();
            RecordingActivity.this.finish();
        } else {
            if (!nfcAndroidAdapter.isEnabled()) {
                // NFC is disabled
                Toast.makeText(RecordingActivity.this, "Please Enable NFC on your Phone!",
                        Toast.LENGTH_LONG).show();
            } else {
                nfcIntent = PendingIntent.getActivity(RecordingActivity.this,
                        0, new Intent(RecordingActivity.this, RecordingActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
            }
        }

        initBeaconManager();

        clearDataBtn.setOnClickListener(v -> {

            if(btnState==SCANNING_OFF) {
                if (!(positionData.isEmpty())) {
                    recordedPositionListAdapter.clear();
                }
                saveRecordingSessionBtn.setVisibility(View.INVISIBLE);
                swipeButton.setVisibility(View.INVISIBLE);
            }else Toast.makeText(RecordingActivity.this,"Scanning has to be stopped first!", Toast.LENGTH_SHORT).show();
        });

        startBtn.setOnClickListener(v -> {

            if (btnState == SCANNING_OFF) {

                saveRecordingSessionBtn.setVisibility(View.INVISIBLE);
                if (receivedSignals != null) receivedSignals.clear();
                if (recordedPositionListAdapter != null) recordedPositionListAdapter.clear();

                startBtn.setText("Pause Recording");

                swipeButton.setEnabled(true);
                swipeButton.collapseButton();
                swipeButton.setVisibility(View.VISIBLE);

                swipeButton.setListener(status -> {

                    if (status) {
                        endRecording();
                    }
                });

                progressBar.setVisibility(View.VISIBLE);

                beaconManager.connect(() -> beaconManager.startEddystoneDiscovery());

                btnState = SCANNING_ON;

            } else if (btnState == SCANNING_ON) {

                beaconManager.stopEddystoneDiscovery();
                beaconManager.disconnect();
                startBtn.setText("Resume Recording");
                progressBar.setVisibility(View.INVISIBLE);

                btnState= SCANNING_PAUSED;

            } else if (btnState == SCANNING_PAUSED) {

                initBeaconManager();
                beaconManager.connect(() -> beaconManager.startEddystoneDiscovery());
                progressBar.setVisibility(View.VISIBLE);

            }
        });

        saveRecordingSessionBtn.setOnClickListener(v -> {

            if (StartActivity.isNetworkAvailable(RecordingActivity.this)){
                openAlertDialog();
            }else  openSaveRecordingSessionDialog();

        });
    }

    private void endRecording() {
        startBtn.setText(R.string.start_recording);
        beaconManager.stopEddystoneDiscovery();

        List<RecordingDataSet> strongestSignals = getStrongestSignals(receivedSignals);

        positionData.addAll(getCoordinatesViaTriangulation(strongestSignals));

        positionData.sort((o1, o2) -> o1.getTimestamp().compareTo(o2.getTimestamp()));

        if (!(positionData.isEmpty())) {
            startTime = positionData.get(0).getTimestamp();
            endTime = positionData.get(positionData.size() - 1).getTimestamp();

            recordedPositionListAdapter = new RecordedPositionListAdapter(RecordingActivity.this, R.layout.recording_data_layout, positionData);

            progressBar.setVisibility(View.GONE);

            recordedPositionListView.setAdapter(recordedPositionListAdapter);
            saveRecordingSessionBtn.setVisibility(View.VISIBLE);
        } else {
            Toast.makeText(RecordingActivity.this, "Longer recording time needed!", Toast.LENGTH_LONG).show();
            progressBar.setVisibility(View.GONE);
        }

        swipeButton.setEnabled(false);

        for (RecordedPosition position : positionData) {

            Log.e("TestingProgram", "Zeit: " + position.getTimestamp() + ", x: " + position.getxCoordinate() + ", y:" + position.getyCoordinate());

        }

        btnState = SCANNING_OFF;
    }

    @Override
    protected void onPause() {
        super.onPause();
        nfcAndroidAdapter.disableForegroundDispatch(this);
        //beaconManager.disconnect();
    }

    @Override
    public void onResume() {
        super.onResume();


        if (nfcAndroidAdapter != null) nfcAndroidAdapter
                .enableForegroundDispatch(RecordingActivity.this, nfcIntent, null,
                        null);

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        String action = intent.getAction();

        if (btnState == SCANNING_ON) {

            System.out.println("FUCKING: " + action);

            if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction())) {

                Parcelable[] rawMsgs = intent.getParcelableArrayExtra(
                        NfcAdapter.EXTRA_NDEF_MESSAGES);
                if (rawMsgs != null) {
                    for (Parcelable rawMsg : rawMsgs) {
                        NdefMessage msg = (NdefMessage) rawMsg;
                        DeviceId beaconId = findBeaconId(msg);
                        if (beaconId != null && beaconsMap.containsKey(beaconId.toHexString())) {

                            positionData.add(new RecordedPosition(LocalDateTime.now(),
                                    Objects.requireNonNull(beaconsMap.get(beaconId.toHexString())).getxCoordinate(),
                                    Objects.requireNonNull(beaconsMap.get(beaconId.toHexString())).getyCoordinate(),
                                    true));

                            Toast.makeText(this, "NFC Beacon " + beaconId.toHexString() + " detected!", Toast.LENGTH_LONG).show();

                        } else
                            Toast.makeText(RecordingActivity.this, "Beacon not found. Please add this beacon in the Beacon-Management.", Toast.LENGTH_LONG).show();
                    }
                }
            }


            initBeaconManager();

            beaconManager.connect(() -> beaconManager.startEddystoneDiscovery());
        }
    }

    private void initBeaconManager() {
        beaconManager = new BeaconManager(this);

        beaconManager.setEddystoneListener(eddystones -> {

            for (Eddystone eddystone : eddystones) {

                Log.e("TestOutput 1 & 2", "Timestamp: " + convertToLocalDateViaInstant(eddystone.timestamp).withNano(0) + ", BeaconInstance: " + eddystone.instance + ", Distance 1: " + calculateDistanceFromRssi(eddystone.rssi, eddystone.calibratedTxPower));

                //Only Estimote Beacons will be considered, therefore we filter with the unique Estimote Namespace
                if (eddystone.namespace.equals("edd1ebeac04e5defa017")) {
                    receivedSignals.add(new ReceivedSignal(eddystone.instance,
                            convertToLocalDateViaInstant(eddystone.timestamp).withNano(0),
                            eddystone.rssi,
                            calculateDistanceFromRssi(eddystone.rssi, eddystone.calibratedTxPower),
                            Objects.requireNonNull(beaconsMap.get(eddystone.instance)).getxCoordinate(),
                            Objects.requireNonNull(beaconsMap.get(eddystone.instance)).getyCoordinate()));
                }
            }
        });
    }

    private static DeviceId findBeaconId(NdefMessage msg) {
        NdefRecord[] records = msg.getRecords();
        for (NdefRecord record : records) {
            if (record.getTnf() == NdefRecord.TNF_EXTERNAL_TYPE) {
                String type = new String(record.getType(), StandardCharsets.US_ASCII);
                if ("estimote.com:id".equals(type)) {
                    return DeviceId.fromBytes(record.getPayload());
                }
            }
        }
        return null;
    }


    private LocalDateTime convertToLocalDateViaInstant(Date dateToConvert) {
        return dateToConvert.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
    }

    private List<RecordingDataSet> getStrongestSignals(List<ReceivedSignal> receivedSignals) {

        List<RecordingDataSet> recordingDataSets = new ArrayList<>();

        Map<LocalDateTime, Set<ReceivedSignal>> timeToSignalsMap = createTimeToSignalsMap(receivedSignals);

        for (Map.Entry<LocalDateTime, Set<ReceivedSignal>> signalsOfTimestamp : timeToSignalsMap.entrySet()) {

            //Strongest Signals per Instance for one Timestamp
            Map<String, ReceivedSignal> strongestSignalPerInstance = getStrongestSignalPerInstance(signalsOfTimestamp.getValue());
            Set<ReceivedSignal> strongestSignalsPerTimestamp = filterThreeStrongestSignals(strongestSignalPerInstance.values());

            if (strongestSignalsPerTimestamp.size() != 3) {
                continue;
            }

            recordingDataSets.add(new RecordingDataSet(signalsOfTimestamp.getKey(), strongestSignalsPerTimestamp));

        }

        return recordingDataSets;
    }

    private Set<ReceivedSignal> filterThreeStrongestSignals(Collection<ReceivedSignal> receivedSignals) {

        return receivedSignals
                .stream()
                .sorted(new ReceivedSignalRSSIComparator())
                .limit(3)
                .collect(Collectors.toSet());
    }

    @NotNull
    private Map<String, ReceivedSignal> getStrongestSignalPerInstance(Set<ReceivedSignal> receivedSignals) {
        Map<String, ReceivedSignal> strongestSignalPerInstance = new HashMap<>();

        for (ReceivedSignal receivedSignal : receivedSignals) {

            String instance = receivedSignal.getInstance();
            ReceivedSignal currentStrongestSignal = strongestSignalPerInstance.get(instance);

            if (currentStrongestSignal == null || receivedSignal.getRssi() > currentStrongestSignal.getRssi()) {

                strongestSignalPerInstance.put(instance, receivedSignal);

            }
        }
        return strongestSignalPerInstance;
    }

    /**
     * Creates a map of timestamps to the receivedSignals of these timestamps.
     *
     * @param receivedSignals a List of ReceivedSignals
     * @return Map of Timestamp to ReceivedSignals
     */

    private Map<LocalDateTime, Set<ReceivedSignal>> createTimeToSignalsMap(List<ReceivedSignal> receivedSignals) {

        Map<LocalDateTime, Set<ReceivedSignal>> timeToSignalsMap = new HashMap<>();

        for (ReceivedSignal receivedSignal : receivedSignals) {

            LocalDateTime timestamp = receivedSignal.getTimestamp();

            Set<ReceivedSignal> signalSet = timeToSignalsMap.get(timestamp);

            if (signalSet == null) {
                signalSet = new HashSet<>();
            }

            signalSet.add(receivedSignal);

            timeToSignalsMap.put(timestamp, signalSet);
        }

        return timeToSignalsMap;
    }


    private double calculateDistanceFromRssi(int rssi, int txPower0m) {

        int pathLoss = txPower0m - rssi;
        return Math.pow(10, (pathLoss - 41) / 20.0);
    }

/*    protected static double calculateDistance(int measuredPowerAtZeroMeters, double rssi) {
        if (rssi == 0) {
            return -1.0; // if we cannot determine distance, return -1.
        }
        double ratio = rssi*1.0/(measuredPowerAtZeroMeters-41);
        if (ratio < 1.0) {
            return Math.pow(ratio,10);
        }
        else {
            double distance =  (0.89976)*Math.pow(ratio,7.7095) + 0.111;
            return distance;
        }
    }*/


/*    private double getDistance(int rssi, int txPower) {
        return (Math.pow(10d, ((double) txPower - rssi) / (10 * 4))) / 10;
    }*/


    private List<RecordedPosition> getCoordinatesViaTriangulation(List<RecordingDataSet> recordingDataSets) {

        double distance1, distance2, distance3;

        distance1 = 0;
        distance2 = 0;
        distance3 = 0;

        double x1, y1, x2, y2, x3, y3;

        x1 = 0;
        y1 = 0;
        x2 = 0;
        y2 = 0;
        x3 = 0;
        y3 = 0;

        double[][] positions;
        double[] distances;

        int counter = 1;

        List<RecordedPosition> recordedPositions = new ArrayList<>();

        for (RecordingDataSet recordingDataSet : recordingDataSets) {

            for (ReceivedSignal receivedSignal : recordingDataSet.getStrongestSignals()) {

                switch (counter) {
                    case 1:
                        distance1 = receivedSignal.getDistance();
                        x1 = receivedSignal.getX();
                        y1 = receivedSignal.getY();
                        break;
                    case 2:
                        distance2 = receivedSignal.getDistance();
                        x2 = receivedSignal.getX();
                        y2 = receivedSignal.getY();
                        break;
                    case 3:
                        distance3 = receivedSignal.getDistance();
                        x3 = receivedSignal.getX();
                        y3 = receivedSignal.getY();
                        break;
                }

                counter++;
            }

            positions = new double[][]{{x1, y1}, {x2, y2}, {x3, y3}};
            distances = new double[]{distance1, distance2, distance3};

            NonLinearLeastSquaresSolver solver = new NonLinearLeastSquaresSolver(new TrilaterationFunction(positions, distances), new LevenbergMarquardtOptimizer());
            LeastSquaresOptimizer.Optimum optimum = solver.solve();

            // the answer
            double[] centroid = optimum.getPoint().toArray();

            recordedPositions.add(new RecordedPosition(recordingDataSet.getTimestamp(), centroid[0], centroid[1], false));

            counter=1;
        }
        return recordedPositions;
    }

    private void openSaveRecordingSessionDialog() {
        SaveRecordingSessionDialog saveRecordingSessionDialog = new SaveRecordingSessionDialog(userId, name, surname, startTime, endTime);
        saveRecordingSessionDialog.show(getSupportFragmentManager(), "AddUserDialog");
    }

    public void saveRecordingSession() {
        Session session = new Session(0, userId, startTime, endTime);
        recordingViewModel.addSession(userId, session, positionData);
    }

    private void initNfcAdapter() {
        final NfcManager nfcManager = (NfcManager) getSystemService(Context.NFC_SERVICE);
        assert nfcManager != null;
        nfcAndroidAdapter = nfcManager.getDefaultAdapter();
    }


    private void openAlertDialog() {
        new AlertDialog.Builder(RecordingActivity.this)
                .setTitle("Please check your Internet Connection")
                .setMessage("This app requires internet access at the time of startup, " +
                        "as well as when trying to save a recorded session.\n" +
                        "Please turn on your internet connection and try again.")

                .setPositiveButton(android.R.string.ok, (dialog, which) -> {

                })

                // A null listener allows the button to dismiss the dialog and take no further action.
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }


}