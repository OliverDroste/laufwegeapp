package com.example.estimotenfc.user.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.example.estimotenfc.R;
import com.example.estimotenfc.user.data.User;
import com.example.estimotenfc.user.dialog.AddUserDialog;
import com.example.estimotenfc.user.dialog.UpdateDeleteUserDialog;
import com.example.estimotenfc.user.viewmodel.UserViewModel;

import java.util.ArrayList;

public class UserManagementActivity extends AppCompatActivity implements AddUserDialog.AddUserDialogListener, UpdateDeleteUserDialog.UpdateDeleteUserDialogListener {

    private UserViewModel userViewModel;
    private UserAdapter userAdapter;

    private ProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_management);

        progressBar=findViewById(R.id.userManagementActivityLoadingBar);
        ListView userListView = findViewById(R.id.existingUsersLv);
        ArrayList<User> userList = new ArrayList<>();
        userAdapter = new UserAdapter(this, userList);
        userListView.setAdapter(userAdapter);

        userViewModel= new ViewModelProvider(this).get(UserViewModel.class);

        userViewModel.getUsers().observe(this,users -> {

            userAdapter.clear();
            progressBar.setVisibility(View.GONE);
            userAdapter.addAll(users);

        });

        Button addNewUserBtn = findViewById(R.id.addNewUserBtn);

        addNewUserBtn.setOnClickListener(v -> openAddUserDialog());

        userListView.setOnItemClickListener((parent, view, position, id) -> {
            // Get the selected item text from ListView
            int selectedUser = Integer.parseInt(((TextView) view.findViewById(R.id.userIdSpTv)).getText().toString());
            String name =  ((TextView) view.findViewById(R.id.nameSpTv)).getText().toString();
            String surname =  ((TextView) view.findViewById(R.id.surnameSpTv)).getText().toString();

            openUpdateDeleteUserDialog(selectedUser, name, surname);

        });


    }

    private void openAddUserDialog() {

        AddUserDialog addUserDialog = new AddUserDialog();
        addUserDialog.show(getSupportFragmentManager(),"AddUserDialog");

    }

    private void openUpdateDeleteUserDialog(int userId, String name, String surname ){

        UpdateDeleteUserDialog updateDeleteUserDialog = new UpdateDeleteUserDialog(userId,name,surname);
        updateDeleteUserDialog.show(getSupportFragmentManager(),"UpdateUserDialog");

    }

    @Override
    public void applyTexts(String name, String surname) {
        userViewModel.addUser(name,surname);
    }


    public void updateApplyTexts(int userId, String name, String surname) {
        userViewModel.updateUser(userId, name,surname);
    }


    public void deleteApplyTexts(int userId) {
        userViewModel.deleteUser(userId);
    }

}
