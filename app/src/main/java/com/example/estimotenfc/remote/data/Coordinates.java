package com.example.estimotenfc.remote.data;


import java.sql.Timestamp;

class Coordinates {

   private int coordinatesId;
   private Timestamp timestamp;
   private int xCoordinate;
   private int yCoordinate;
   private boolean isMachine;
   private int sessionId;

    public Coordinates(int coordinatesId, Timestamp timestamp, int xCoordinate, int yCoordinate, boolean isMachine, int sessionId) {
        this.coordinatesId = coordinatesId;
        this.timestamp = timestamp;
        this.xCoordinate = xCoordinate;
        this.yCoordinate = yCoordinate;
        this.isMachine = isMachine;
        this.sessionId = sessionId;
    }

    public int getCoordinatesId() {
        return coordinatesId;
    }

    public void setCoordinatesId(int coordinatesId) {
        this.coordinatesId = coordinatesId;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public int getxCoordinate() {
        return xCoordinate;
    }

    public void setxCoordinate(int xCoordinate) {
        this.xCoordinate = xCoordinate;
    }

    public int getyCoordinate() {
        return yCoordinate;
    }

    public void setyCoordinate(int yCoordinate) {
        this.yCoordinate = yCoordinate;
    }

    public boolean getIsMachine() {
        return isMachine;
    }

    public void setIsMachine(boolean machine) {
        isMachine = machine;
    }

    public int getSessionId() {
        return sessionId;
    }

    public void setSessionId(int sessionId) {
        this.sessionId = sessionId;
    }
}
