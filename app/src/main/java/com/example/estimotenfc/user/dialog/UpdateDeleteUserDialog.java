package com.example.estimotenfc.user.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.example.estimotenfc.R;

import java.util.Objects;

public class UpdateDeleteUserDialog extends AppCompatDialogFragment {

    private UpdateDeleteUserDialogListener listener;

    public UpdateDeleteUserDialog(int userId, String name, String surname) {
        this.userId = userId;
        this.name = name;
        this.surname = surname;
    }

    private final int userId;
    private final String name;
    private final String surname;
    private EditText editTextName;
    private EditText editTextSurname;
    private TextView textViewUserId;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = Objects.requireNonNull(getActivity()).getLayoutInflater();
        View view = inflater.inflate(R.layout.update_delete_popup_layout,null);

        builder.setView(view)
                .setTitle("Update or Delete User")
                .setNegativeButton("Cancel", (dialog, which) -> {

                })
                .setPositiveButton("Update", (dialog, which) -> {
                    int userId = Integer.parseInt(textViewUserId.getText().toString());
                    String name = editTextName.getText().toString();
                    String surname = editTextSurname.getText().toString();
                    listener.updateApplyTexts(userId, name,surname);
                })
                .setNeutralButton("Delete", (dialog, which) -> listener.deleteApplyTexts(userId));

        textViewUserId = view.findViewById(R.id.userIdUpdateDeletePopupTv);
        textViewUserId.setText(Integer.toString(userId));
        editTextName = view.findViewById(R.id.nameUpdateDeletePopupTv);
        editTextSurname = view.findViewById(R.id.surnameUpdateDeletePopupTv);
        editTextName.setText(name);
        editTextSurname.setText(surname);

        return builder.create();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        try {
            listener = (UpdateDeleteUserDialog.UpdateDeleteUserDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "must implement ExampleDialogListener");
        }

    }


    public interface UpdateDeleteUserDialogListener{

        void updateApplyTexts(int userId, String name, String surname);
        void deleteApplyTexts(int userId);
    }

}
