package com.example.estimotenfc.recording.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.estimotenfc.R;
import com.example.estimotenfc.recording.data.RecordedPosition;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

class RecordedPositionListAdapter extends ArrayAdapter<RecordedPosition> {

    private final Context context;
    private final int ressoure;
    RecordedPositionListAdapter(Context context, int resource, List<RecordedPosition> objects) {
        super(context, resource, objects);
        this.context = context;
        this.ressoure=resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        //get the RecordedPosition information

        LocalDateTime timestamp = Objects.requireNonNull(getItem(position)).getTimestamp();
        double xCoordinate = Objects.requireNonNull(getItem(position)).getxCoordinate();
        double yCoordinate = Objects.requireNonNull(getItem(position)).getyCoordinate();
        boolean isMachine = Objects.requireNonNull(getItem(position)).getIsMachine();

        RecordedPosition recordedPosition = new RecordedPosition(timestamp,xCoordinate,yCoordinate,isMachine);

        LayoutInflater inflater = LayoutInflater.from(context);
        convertView = inflater.inflate(ressoure,parent,false);

        TextView tvTimestamp = convertView.findViewById(R.id.recordingDataTimestampTv);
        TextView tvXCoordinate = convertView.findViewById(R.id.recordingDataXCoordinateTv);
        TextView tvYCoordinate = convertView.findViewById(R.id.recordingDataYCoordinateTv);
        TextView tvIsMachine = convertView.findViewById(R.id.recordingDataIsMachineTv);



        tvTimestamp.setText(timestamp.toString());
        tvXCoordinate.setText(Double.toString(Math.round(xCoordinate * 100.0) / 100.0));
        tvYCoordinate.setText(Double.toString(Math.round(yCoordinate * 100.0) / 100.0));
        if(isMachine){
            tvIsMachine.setText(R.string.nfc);
        }else tvIsMachine.setText(R.string.ble);

        return convertView;
    }

}
