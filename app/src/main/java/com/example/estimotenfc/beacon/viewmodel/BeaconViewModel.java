package com.example.estimotenfc.beacon.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.estimotenfc.beacon.data.Beacon;
import com.example.estimotenfc.remote.api.JsonApi;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BeaconViewModel extends ViewModel {

    private final String BASEURL = "https://springbootapp-1593292906275.azurewebsites.net";

    private final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BASEURL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    private final JsonApi jsonApi = retrofit.create(JsonApi.class);

    private MutableLiveData<List<Beacon>> beacons;

    public LiveData<List<Beacon>> getBeacons() {

        if(beacons==null){
            beacons = new MutableLiveData<>();
            loadBeacons();
        }
        return beacons;
    }

    private void loadBeacons() {

        Call<List<Beacon>> call = jsonApi.getBeacons();

        call.enqueue(new Callback<List<Beacon>>() {
            @Override
            public void onResponse(Call<List<Beacon>> call, Response<List<Beacon>> response) {

                if(!response.isSuccessful()) {
                    System.out.println(response.code());
                }else {

                    beacons.postValue(response.body());

                }
            }

            @Override
            public void onFailure(Call<List<Beacon>> call, Throwable t) {
                System.out.println(t.getMessage());
            }
        });
    }


    public void deleteBeacon(String instance) {

        Call<Void> call = jsonApi.deleteBeacon(instance);

        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                System.out.println("Code: " + response.code());
                if(response.code()==200){
                    loadBeacons();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                System.out.println(t.getMessage());
            }
        });



    }

    public void updateBeacon(String instance, String xCoordinate, String yCoordinate) {

        Beacon beacon = new Beacon(instance,Double.parseDouble(xCoordinate),Double.parseDouble(yCoordinate),false);

        Call<Void> call = jsonApi.updateBeacon(beacon);

        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                System.out.println("Code: " + response.code());
                if(response.code()==200){
                    loadBeacons();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                System.out.println(t.getMessage());
            }
        });



    }

    public void addBeacon(String instance, String xCoordinate, String yCoordinate, boolean isMachine) {

        Beacon beacon = new Beacon(instance,Double.parseDouble(xCoordinate),Double.parseDouble(yCoordinate),isMachine);

        Call<Void> call = jsonApi.updateBeacon(beacon);

        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                System.out.println("Code: " + response.code());
                if(response.code()==200){
                    loadBeacons();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                System.out.println(t.getMessage());
            }
        });



    }


}
