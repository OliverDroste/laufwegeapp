package com.example.estimotenfc.start;

import android.Manifest;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.example.estimotenfc.R;
import com.example.estimotenfc.beacon.ui.BeaconManagementActivity;
import com.example.estimotenfc.recording.ui.RecordingActivity;
import com.example.estimotenfc.user.data.User;
import com.example.estimotenfc.user.ui.UserAdapter;
import com.example.estimotenfc.user.ui.UserManagementActivity;
import com.example.estimotenfc.user.viewmodel.UserViewModel;

import java.util.ArrayList;
import java.util.Objects;

public class StartActivity extends AppCompatActivity {

    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;
    private static final int PERMISSION_REQUEST_FINE_LOCATION = 1;
    private static final int REQUEST_ENABLE_BT = 0;

    private Button laufwegeAufzeichnenBtn;
    private UserAdapter userAdapter;
    private UserViewModel userViewModel;
    private int userId;
    private String name;
    private String surname;
    private int savedUserId;
    private SharedPreferences sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        sharedPref = StartActivity.this.getPreferences(Context.MODE_PRIVATE);
        savedUserId = sharedPref.getInt(getString(R.string.preference_file__key), userId);

        if (isNetworkAvailable(this)){
            openAlertDialog();
        }

        ArrayList<User> userList = new ArrayList<>();
        Spinner userSpinner = findViewById(R.id.userIdSpinner);
        userAdapter = new UserAdapter(this, userList);

        userSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                User selectedUser = (User) parent.getItemAtPosition(position);
                userId = selectedUser.getUserId();
                name = selectedUser.getName();
                surname = selectedUser.getSurname();

                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putInt(getString(R.string.preference_file__key), userId);
                editor.commit();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        userSpinner.setAdapter(userAdapter);




        userViewModel= new ViewModelProvider(this).get(UserViewModel.class);
        userViewModel.getUsers().observe(this,users -> {

            userAdapter.clear();
            userAdapter.addAll(users);


            for(User user : userList){
                if(savedUserId == user.getUserId()){
                    userSpinner.setSelection(userAdapter.getPosition(user));
                }
            }

            laufwegeAufzeichnenBtn.setAlpha(1);
            laufwegeAufzeichnenBtn.setOnClickListener(v -> openRecordingActivity());

        });




        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            Toast.makeText(StartActivity.this,"This Device does not support Bluetooth!",Toast.LENGTH_LONG).show();
        }

        if (!Objects.requireNonNull(mBluetoothAdapter).isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }


        if (this.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("This app needs location access");
            builder.setMessage("Please grant location access so this app can detect peripherals.");
            builder.setPositiveButton(android.R.string.ok, null);
            builder.setOnDismissListener(dialog -> requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_COARSE_LOCATION));
            builder.show();
        }

        //Check for Coarse Location
        if (this.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("This app needs location access");
            builder.setMessage("Please grant location access so this app can detect peripherals.");
            builder.setPositiveButton(android.R.string.ok, null);
            builder.setOnDismissListener(dialog -> requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_FINE_LOCATION));
            builder.show();
        }

        Button beaconManagementBtn = findViewById(R.id.beaconManagementBtn);
        laufwegeAufzeichnenBtn = findViewById(R.id.laufwegeAufzeichnenBtn);
        Button userManagementBtn = findViewById(R.id.userManagementBtn);
        laufwegeAufzeichnenBtn.setAlpha(0.5f);

        beaconManagementBtn.setOnClickListener(v -> openBeaconManagementActivity());

        userManagementBtn.setOnClickListener(v -> openUserManagementActivity());

    }

    private void openAlertDialog() {
        new AlertDialog.Builder(StartActivity.this)
                .setTitle("Please check your Internet Connection")
                .setMessage("This app requires internet access at the time of startup, " +
                        "as well as when trying to save a recorded session.\n" +
                        "Please fix this issue and restart the app.")

                .setPositiveButton(android.R.string.ok, (dialog, which) -> System.exit(1))

                // A null listener allows the button to dismiss the dialog and take no further action.
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void openBeaconManagementActivity() {

        Intent intent = new Intent(this, BeaconManagementActivity.class);
        startActivity(intent);

    }

    private void openRecordingActivity() {

        Intent intent = new Intent(this, RecordingActivity.class);
        intent.putExtra("EXTRA_USER_ID",userId);
        intent.putExtra("EXTRA_USER_NAME",name);
        intent.putExtra("EXTRA_USER_SURNAME",surname);
        startActivity(intent);

    }

    private void openUserManagementActivity() {

        Intent intent = new Intent(this, UserManagementActivity.class);
        startActivity(intent);
    }


    @Override
    protected void onResume() {
        super.onResume();
        savedUserId = sharedPref.getInt(getString(R.string.preference_file__key), userId);
        userViewModel.loadUsers();
    }

    @Override
    protected void onPause() {
        super.onPause();
        laufwegeAufzeichnenBtn.setAlpha(0.5f);
    }

    public static boolean isNetworkAvailable(Context context) {
        if(context == null)  return true;


        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivityManager != null) {

            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                NetworkCapabilities capabilities = connectivityManager.getNetworkCapabilities(connectivityManager.getActiveNetwork());
                if (capabilities != null) {
                    if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                        return false;
                    } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                        return false;
                    }  else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)){
                        return false;
                    }
                }
            }

            else {

                try {
                    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                    if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
                        Log.i("update_statut", "Network is available : true");
                        return false;
                    }
                } catch (Exception e) {
                    Log.i("update_statut", "" + e.getMessage());
                }
            }
        }
        Log.i("update_statut","Network is available : FALSE ");
        return true;
    }



}
