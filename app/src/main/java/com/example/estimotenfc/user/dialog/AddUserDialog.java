package com.example.estimotenfc.user.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.example.estimotenfc.R;

import java.util.Objects;

public class AddUserDialog extends AppCompatDialogFragment {

    private AddUserDialogListener listener;

    private EditText editTextName;
    private EditText editTextSurname;


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = Objects.requireNonNull(getActivity()).getLayoutInflater();
        View view = inflater.inflate(R.layout.add_user_popup_layout,null);

        builder.setView(view)
                .setTitle("Add User")
                .setNegativeButton("Cancel", (dialog, which) -> {

                })
                .setPositiveButton("Add", (dialog, which) -> {
                    String name = editTextName.getText().toString();
                    String surname = editTextSurname.getText().toString();
                    listener.applyTexts(name,surname);
                });

        editTextName = view.findViewById(R.id.namePoTv);
        editTextSurname = view.findViewById(R.id.surnamePoTv);

        return builder.create();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        try {
            listener = (AddUserDialog.AddUserDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "must implement ExampleDialogListener");
        }

    }


    public interface AddUserDialogListener{
        void applyTexts(String name, String surname);
    }

}
