package com.example.estimotenfc.recording.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.example.estimotenfc.R;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public class SaveRecordingSessionDialog extends AppCompatDialogFragment {

    private SaveRecordingSessionDialogListener listener;

    public SaveRecordingSessionDialog (int userId, String name, String surname, LocalDateTime startTime, LocalDateTime endTime){
        this.userId = userId;
        this.startTime=startTime;
        this.endTime = endTime;
        this.name=name;
        this.surname=surname;
    }

    private final int userId;
    private final String name;
    private final String surname;
    private final LocalDateTime startTime;
    private final LocalDateTime endTime;


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = Objects.requireNonNull(getActivity()).getLayoutInflater();
        View view = inflater.inflate(R.layout.save_recording_popup_layout,null);

        builder.setView(view)
                .setTitle("Save this session for the following user?")
                .setNegativeButton("Cancel", (dialog, which) -> {

                })
                .setPositiveButton("Save", (dialog, which) -> {
                    /*int userId = Integer.parseInt(userIdTv.getText().toString());
                    Timestamp startDate = Timestamp.valueOf(startTimeTv.getText().toString());
                    Timestamp endDate = Timestamp.valueOf(endTimeTv.getText().toString());*/

                    listener.saveRecordingSession();
                });

        TextView userIdTv = view.findViewById(R.id.saveRecordingSessionPopupUserIdTv);
        TextView startTimeTv = view.findViewById(R.id.saveRecordingSessionPopupStartTimeTv);
        TextView endTimeTv = view.findViewById(R.id.saveRecordingSessionEndTimeTv);

        String userInformation = userId + ", " + name + " " + surname;

        userIdTv.setText(userInformation);

        startTimeTv.setText(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(startTime));
        endTimeTv.setText(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(endTime));

        return builder.create();
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        try {
            listener = (SaveRecordingSessionDialog.SaveRecordingSessionDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "must implement ExampleDialogListener");
        }

    }


   public interface SaveRecordingSessionDialogListener {
        void saveRecordingSession();
    }



}
