package com.example.estimotenfc.beacon.ui;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcManager;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.estimote.coresdk.recognition.packets.Eddystone;
import com.estimote.coresdk.recognition.utils.DeviceId;
import com.estimote.coresdk.service.BeaconManager;
import com.example.estimotenfc.R;
import com.example.estimotenfc.beacon.data.Beacon;
import com.example.estimotenfc.beacon.dialog.AddBeaconDialog;
import com.example.estimotenfc.beacon.dialog.UpdateDeleteBeaconDialog;
import com.example.estimotenfc.beacon.viewmodel.BeaconViewModel;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class BeaconManagementActivity extends AppCompatActivity implements AddBeaconDialog.AddBeaconDialogListener, UpdateDeleteBeaconDialog.UpdateDeleteBeaconDialogListener {

    private final int SCANNING_ON = 1;
    private final int SCANNING_OFF = 0;

    private BeaconViewModel beaconViewModel;
    private Map<String, Beacon> beaconsMap;

    private NfcAdapter nfcAndroidAdapter;
    private PendingIntent nfcIntent;

    private ProgressBar scanningBar;
    private ProgressBar progressBar;
    TextView beaconInstanceTv;
    String newXCoordinate;
    String newYCoordinate;

    // 1 = Scanning is stopped at the moment
    // 2 = Scanning is happening at the moment
    private int scanButtonState;

    private ArrayAdapter<String> listAdapter;
    private BeaconListAdapter beaconListAdapter;
    private List<Beacon> beaconList;

    ArrayList<Eddystone> foundEddystones;
    private BeaconManager beaconManager;

    private final String Digits = "(\\p{Digit}+)";
    private final String HexDigits = "(\\p{XDigit}+)";
    // an exponent is 'e' or 'E' followed by an optionally
    // signed decimal integer.
    private final String Exp = "[eE][+-]?" + Digits;
    private final String fpRegex =
            ("[\\x00-\\x20]*" + // Optional leading "whitespace"
                    "[+-]?(" +         // Optional sign character
                    "NaN|" +           // "NaN" string
                    "Infinity|" +      // "Infinity" string

                    // A decimal floating-point string representing a finite positive
                    // number without a leading sign has at most five basic pieces:
                    // Digits . Digits ExponentPart FloatTypeSuffix
                    //
                    // Since this method allows integer-only strings as input
                    // in addition to strings of floating-point literals, the
                    // two sub-patterns below are simplifications of the grammar
                    // productions from the Java Language Specification, 2nd edition

                    // Digits ._opt Digits_opt ExponentPart_opt FloatTypeSuffix_opt
                    "(((" + Digits + "(\\.)?(" + Digits + "?)(" + Exp + ")?)|" +

                    // . Digits ExponentPart_opt FloatTypeSuffix_opt
                    "(\\.(" + Digits + ")(" + Exp + ")?)|" +

                    // Hexadecimal strings
                    "((" +
                    // 0[xX] HexDigits ._opt BinaryExponent FloatTypeSuffix_opt
                    "(0[xX]" + HexDigits + "(\\.)?)|" +

                    // 0[xX] HexDigits_opt . HexDigits BinaryExponent FloatTypeSuffix_opt
                    "(0[xX]" + HexDigits + "?(\\.)" + HexDigits + ")" +

                    ")[pP][+-]?" + Digits + "))" +
                    "[fFdD]?))" +
                    "[\\x00-\\x20]*");// Optional trailing "whitespace"

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beacon_management);

        scanButtonState = SCANNING_OFF;
        progressBar = findViewById(R.id.beaconManagementActivityLoadingBar);
        scanningBar = findViewById(R.id.beaconManagementActivityScanningBar);

        beaconViewModel = new ViewModelProvider(this).get(BeaconViewModel.class);

        beaconViewModel.getBeacons().observe(this, beacons -> {

            beaconListAdapter.clear();
            progressBar.setVisibility(View.GONE);
            beaconListAdapter.addAll(beacons);

            beaconsMap.clear();
            for (Beacon beacon : beacons) {
                beaconsMap.put(beacon.getBeaconId(), beacon);
            }

        });


        beaconList = new ArrayList<>();
        beaconsMap = new HashMap<>();

        ListView savedBeaconsLv = findViewById(R.id.savedBeaconsLv);


        listAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1);
        ListView foundBeaconsLv = findViewById(R.id.foundBeaconsLv);
        foundBeaconsLv.setAdapter(listAdapter);

        beaconListAdapter = new BeaconListAdapter(this, R.layout.adapter_view_layout, beaconList);
        savedBeaconsLv.setAdapter(beaconListAdapter);

        beaconManager = new BeaconManager(this);
        beaconManager.setEddystoneListener(eddystones -> {

            for (Eddystone eddystone : eddystones) {

                //Only Estimote Beacons will be considered, therefore we filter with the unique Estimote Namespace
                if (eddystone.namespace.equals("edd1ebeac04e5defa017")) {

                    if (eddystone.instance != null && (isDuplicate(eddystone))) {
                        if (isAlreadySaved(eddystone)) {
                            synchronized (eddystone) {
                                listAdapter.add(eddystone.instance);
                            }
                        }
                    }

                }

            }
        });


        initNfcAdapter();

        if (nfcAndroidAdapter == null) {
            // Device does not support NFC
            Toast.makeText(BeaconManagementActivity.this,
                    "Device does not support NFC!",
                    Toast.LENGTH_LONG).show();
            BeaconManagementActivity.this.finish();
        } else {
            if (!nfcAndroidAdapter.isEnabled()) {
                // NFC is disabled
                Toast.makeText(BeaconManagementActivity.this, "Please Enable NFC on your Phone!",
                        Toast.LENGTH_LONG).show();
            } else {
                nfcIntent = PendingIntent.getActivity(BeaconManagementActivity.this,
                        0, new Intent(BeaconManagementActivity.this, BeaconManagementActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
            }
        }

        Button scanForNewBeaconsBtn = findViewById(R.id.scanForNewBeaconsBtn);
        scanForNewBeaconsBtn.setOnClickListener(v -> {


            if (scanButtonState == SCANNING_OFF) {

                scanningBar.setVisibility(View.VISIBLE);
                scanButtonState = SCANNING_ON;
                scanForNewBeaconsBtn.setText(R.string.stop_scanning);

                beaconManager.connect(() -> {
                    // Eddystone scanning.

                    listAdapter.clear();
                    beaconManager.startEddystoneDiscovery();
                });
            } else if (scanButtonState == SCANNING_ON) {

                scanningBar.setVisibility(View.GONE);
                scanButtonState = SCANNING_OFF;
                scanForNewBeaconsBtn.setText(R.string.scan_for_new_beacons);

                beaconManager.stopEddystoneDiscovery();

            }
        });

        foundBeaconsLv.setOnItemClickListener((parent, view, position, id) -> {
            // Get the selected item text from ListView

            if (scanButtonState == SCANNING_OFF) {
                String selectedBeacon = (String) parent.getItemAtPosition(position);
                initialOpenAddBeaconDialog(selectedBeacon, false);
            } else {
                Toast.makeText(BeaconManagementActivity.this, "Stop the scan to add Beacons.",
                        Toast.LENGTH_LONG).show();
            }

        });

        savedBeaconsLv.setOnItemClickListener((parent, view, position, id) -> {
            // Get the selected item text from ListView
            String selectedBeacon = ((TextView) view.findViewById(R.id.beaconInstanceTv)).getText().toString();
            String xCoordinate = ((TextView) view.findViewById(R.id.xCoordinateTv)).getText().toString();
            String yCoordinate = ((TextView) view.findViewById(R.id.yCoordinateTv)).getText().toString();

            openUpdateDeleteBeaconDialog(selectedBeacon, xCoordinate, yCoordinate);

        });


    }

    private void initialOpenAddBeaconDialog(String selectedBeacon, boolean isMachine) {

        AddBeaconDialog addBeaconDialog = new AddBeaconDialog(selectedBeacon, isMachine);
        addBeaconDialog.show(getSupportFragmentManager(), "AddBeaconDialog");

    }

    private void afterErrorOpenAddBeaconDialog(String selectedBeacon, String xCoordinate, String yCoordinate, boolean isMachine) {

        AddBeaconDialog addBeaconDialog = new AddBeaconDialog(selectedBeacon, xCoordinate, yCoordinate, isMachine);
        addBeaconDialog.show(getSupportFragmentManager(), "AddBeaconDialog");

    }

    private void openUpdateDeleteBeaconDialog(String selectedBeacon, String xCoordinate, String yCoordinate) {

        UpdateDeleteBeaconDialog updateDeleteBeaconDialog = new UpdateDeleteBeaconDialog(selectedBeacon, xCoordinate, yCoordinate);
        updateDeleteBeaconDialog.show(getSupportFragmentManager(), "UpdateDeleteBeaconDialog");

    }


    //Scanning Logic
    private boolean isAlreadySaved(Eddystone eddystone) {
        for (int i = 0; i < beaconList.size(); i++) {
            String addedDeviceDetail = beaconList.get(i).getBeaconId();
            if (addedDeviceDetail.equals(eddystone.instance)) {
                return false;
            }
        }
        return true;
    }

    private boolean isDuplicate(Eddystone eddystone) {
        for (int i = 0; i < listAdapter.getCount(); i++) {
            String addedDeviceDetail = listAdapter.getItem(i);
            assert addedDeviceDetail != null;
            if (addedDeviceDetail.equals(eddystone.instance)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void applyTexts(String instance, String xCoordinate, String yCoordinate, boolean isMachine) {

        if (Pattern.matches(fpRegex, xCoordinate) && (Pattern.matches(fpRegex, yCoordinate))) {
            beaconViewModel.addBeacon(instance, xCoordinate, yCoordinate, isMachine);
            if (!isMachine) listAdapter.remove(instance);
        } else {
            Toast.makeText(BeaconManagementActivity.this, "Please input a valid number!", Toast.LENGTH_LONG).show();
            afterErrorOpenAddBeaconDialog(instance, xCoordinate, yCoordinate, isMachine);
        }
    }

    public void updateApplyTexts(String instance, String xCoordinate, String yCoordinate, String originalXCoordinate, String originalYCoordinate) {

        if (Pattern.matches(fpRegex, xCoordinate) && (Pattern.matches(fpRegex, yCoordinate))) {
            beaconViewModel.updateBeacon(instance, xCoordinate, yCoordinate);
        } else {
            Toast.makeText(BeaconManagementActivity.this, "Please input a valid number!", Toast.LENGTH_LONG).show();
            openUpdateDeleteBeaconDialog(instance, originalXCoordinate, originalYCoordinate);
        }
    }

    public void deleteApplyTexts(String instance) {

        beaconViewModel.deleteBeacon(instance);

    }

    private void initNfcAdapter() {
        final NfcManager nfcManager = (NfcManager) getSystemService(Context.NFC_SERVICE);
        assert nfcManager != null;
        nfcAndroidAdapter = nfcManager.getDefaultAdapter();
    }

    @Override
    protected void onPause() {
        super.onPause();
        nfcAndroidAdapter.disableForegroundDispatch(this);
        //beaconManager.disconnect();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (nfcAndroidAdapter != null) nfcAndroidAdapter
                .enableForegroundDispatch(BeaconManagementActivity.this, nfcIntent, null,
                        null);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        String action = intent.getAction();

        System.out.println(action);

        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction()) || NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())) {

            Parcelable[] rawMsgs = intent.getParcelableArrayExtra(
                    NfcAdapter.EXTRA_NDEF_MESSAGES);

            System.out.println("RawMSG:" + (rawMsgs != null));

            if (rawMsgs != null) {
                for (Parcelable rawMsg : rawMsgs) {
                    NdefMessage msg = (NdefMessage) rawMsg;
                    DeviceId beaconId = findBeaconId(msg);
                    assert beaconId != null;
                    System.out.println("Vor If-Bedingung mit DeviceID: " + beaconId.toHexString());
                    if (!beaconsMap.containsKey(beaconId.toHexString())) {
                        initialOpenAddBeaconDialog(beaconId.toHexString(), true);
                    } else {
                        Toast.makeText(this, "This NFC Beacon has already been added!", Toast.LENGTH_LONG).show();
                    }
                }
            }
        }

        if (scanButtonState == SCANNING_ON) {
            initBeaconManager();
            beaconManager.connect(() -> beaconManager.startEddystoneDiscovery());
        }


    }


    private void initBeaconManager() {
        beaconManager = new BeaconManager(this);
        beaconManager.setEddystoneListener(eddystones -> {

            for (Eddystone eddystone : eddystones) {

                //Only Estimote Beacons will be considered, therefore we filter with the unique Estimote Namespace
                if (eddystone.namespace.equals("edd1ebeac04e5defa017")) {

                    if (eddystone.instance != null && (isDuplicate(eddystone))) {
                        if (isAlreadySaved(eddystone)) {
                            synchronized (eddystone) {
                                listAdapter.add(eddystone.instance);
                            }
                        }
                    }

                }

            }
        });
    }

    private static DeviceId findBeaconId(NdefMessage msg) {
        NdefRecord[] records = msg.getRecords();
        for (NdefRecord record : records) {
            if (record.getTnf() == NdefRecord.TNF_EXTERNAL_TYPE) {
                String type = new String(record.getType(), StandardCharsets.US_ASCII);
                if ("estimote.com:id".equals(type)) {
                    return DeviceId.fromBytes(record.getPayload());
                }
            }
        }
        return null;
    }


}
