package com.example.estimotenfc.beacon.data;

import org.jetbrains.annotations.NotNull;

public class Beacon {

    private String beaconId;
    private double xCoordinate;
    private double yCoordinate;
    private boolean isMachine;

    public Beacon(String beaconId, double xCoordinate, double yCoordinate, boolean isMachine) {
        this.beaconId = beaconId;
        this.xCoordinate = xCoordinate;
        this.yCoordinate = yCoordinate;
        this.isMachine = isMachine;
    }

    public String getBeaconId() {
        return beaconId;
    }

    public void setBeaconId(String beaconId) {
        this.beaconId = beaconId;
    }

    public double getxCoordinate() {
        return xCoordinate;
    }

    public void setxCoordinate(double xCoordinate) {
        this.xCoordinate = xCoordinate;
    }

    public double getyCoordinate() {
        return yCoordinate;
    }

    public void setyCoordinate(double yCoordinate) {
        this.yCoordinate = yCoordinate;
    }

    public boolean getIsMachine() {
        return isMachine;
    }

    public void setIsMachine(boolean machine) {
        isMachine = machine;
    }

    @NotNull
    @Override
    public String toString() {
        return "Beacon{" +
                "beaconId='" + beaconId + '\'' +
                ", xCoordinate=" + xCoordinate +
                ", yCoordinate=" + yCoordinate +
                ", isMachine=" + isMachine +
                '}';
    }
}
