package com.example.estimotenfc.remote.api;

import com.example.estimotenfc.beacon.data.Beacon;
import com.example.estimotenfc.recording.data.RecordedPosition;
import com.example.estimotenfc.remote.data.Session;
import com.example.estimotenfc.user.data.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface JsonApi {
    @GET("beacon")
    Call<List<Beacon>> getBeacons();

    @DELETE("beacon/{beaconId}")
    Call<Void> deleteBeacon(@Path("beaconId") String beaconId);

    @PUT("beacon")
    Call<Void> updateBeacon(@Body Beacon beacon);

    @POST("beacon")
    Call<Void> addBeacon(@Body Beacon beacon);

    @GET("user")
    Call<List<User>> getUsers();

    @POST("user")
    Call<Void> addUser(@Body User user);

    @DELETE("user/{userId}")
    Call<Void> deleteUser(@Path("userId") int userId);

    @PUT("user")
    Call<Void> updateUser(@Body User user);

    @POST("user/{userId}/session")
    Call<Session> addSession(@Path("userId") int userId, @Body Session session);

    @POST("session/{sessionId}/coordinates")
    Call<Void> addCoordinates(@Path("sessionId") int sessionId, @Body List<RecordedPosition> coordinates);

}
