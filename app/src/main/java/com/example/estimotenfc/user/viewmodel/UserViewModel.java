package com.example.estimotenfc.user.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.estimotenfc.remote.api.JsonApi;
import com.example.estimotenfc.user.data.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UserViewModel extends ViewModel {

    private final String BASEURL = "https://springbootapp-1593292906275.azurewebsites.net";

    private final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BASEURL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    private final JsonApi jsonApi = retrofit.create(JsonApi.class);

    private MutableLiveData<List<User>> users;

    public LiveData<List<User>> getUsers() {

        if (users == null) {
            users = new MutableLiveData<>();
            loadUsers();
        }
        return users;
    }


    public void loadUsers() {

        Call<List<User>> call = jsonApi.getUsers();

        call.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {

                if (!response.isSuccessful()) {
                    System.out.println(response.code());
                } else {

                    users.postValue(response.body());
                    System.out.println("Users:" + users.toString());
                }
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                System.out.println(t.getMessage());
            }
        });
    }


    public void addUser(String name, String surname) {

        User user = new User(0, name, surname);

        Call<Void> call = jsonApi.addUser(user);

        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                System.out.println("Code: " + response.code());
                if (response.code() == 200) {
                    loadUsers();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                System.out.println(t.getMessage());
            }
        });
    }

    public void updateUser(int userId, String name, String surname) {

        User user = new User(userId, name, surname);

        Call<Void> call = jsonApi.updateUser(user);

        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                System.out.println("Code: " + response.code());
                if (response.code() == 200) {
                    loadUsers();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                System.out.println(t.getMessage());
            }
        });
    }

    public void deleteUser(int userId) {

        Call<Void> call = jsonApi.deleteUser(userId);

        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                System.out.println("Code: " + response.code());
                if (response.code() == 200) {
                    loadUsers();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                System.out.println(t.getMessage());
            }
        });
    }

}